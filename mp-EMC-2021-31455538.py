# EMC: 2021
# Nombre del Alumno: Aldo Martin Machaca
# DNI: 31455538

import os

def detectaSO():
    if os.name == 'nt':
        borrar = 'cls'
    else:
        borrar = 'clear'
    return borrar

def limpiarPantalla(limpiar):
    os.system(limpiar)


def validaIngreso(a):
    return len(a) == 0



def validaID(mensaje, diccionario):
    id = input(mensaje)
    while id != 0:
        if id in diccionario:
            print('ID existente')
        else:
            break
        id = input(mensaje)
    return id

def validaID(totalEmpleados):
    id = validaID('ID: ', totalEmpleados)
    while id != 0 :
        nombre = validaIngreso('Nombre: ')
        email = validaIngreso('eMail: ')
        departamento = departamento('Departamento: ')
        stock = validaIngreso('Stock: ')
        totalEmpleados[id] = [nombre, email, departamento]
        print()
        id = validaID('ID: ', totalEmpleados)
    return totalEmpleados

empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]
            ]
# A continuación un ejemplo que muestra la lista. Puede borrarlo            
for e in empleados:    
    print(e)
